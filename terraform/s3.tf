# TODO : Create a s3 bucket with aws_s3_bucket
resource "aws_s3_bucket" "b" {
  bucket = "my-tf-bucket-libert-gidrol-flament"
  force_destroy = true
  tags = {
    Name        = "my-tf-bucket-libert-gidrol-flament"
    Environment = "Dev"
  }
}
# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_object
resource "aws_s3_object" "job_offers" {
    bucket = "${aws_s3_bucket.b.id}"
    key    = "job_offers/raw/"
    source = "/dev/null"
}


# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.b.id}"
  lambda_function {
    lambda_function_arn = aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }
  depends_on = [aws_lambda_permission.test_lambda_s3]
}
